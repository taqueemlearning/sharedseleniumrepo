import org.testng.Assert;
import org.testng.annotations.Test;
import seleniumCore.CoreGroups;
import seleniumCore.CorePage;


public class DemoTest4 extends CorePage {


    @Test(groups = {CoreGroups.smokeTests})
    public void DemoTest4_test1() {
        getDriver().get("https://www.gov.sg/");
        Assert.fail("failed test 2");
    }

    @Test(groups = {CoreGroups.smokeTests, CoreGroups.regressionTest})
    public void DemoTest4_test2() {
        getDriver().get("https://www.amazon.com/");
        Assert.fail("failed test 2");
    }
}
