import org.testng.annotations.Test;
import seleniumCore.CoreGroups;
import seleniumCore.CorePage;


public class DemoTest3 extends CorePage {


    @Test(groups = {CoreGroups.regressionTest})
    public void DemoTest3_test1() {
        getDriver().get("https://www.netflix.com/sg/");
    }


    @Test(groups = {CoreGroups.regressionTest})
    public void DemoTest3_test2() {

        getDriver().get("https://www.lazada.com/");
    }
}
