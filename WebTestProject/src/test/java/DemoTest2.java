import org.testng.Assert;
import org.testng.annotations.Test;
import seleniumCore.CoreGroups;
import seleniumCore.CorePage;
import seleniumCore.DriverFactory;


public class DemoTest2  extends CorePage {


    @Test(groups = {CoreGroups.smokeTests})
    public void DemoTest2_test1() {
        DriverFactory.getDriver().get("https://www.google.com/");
        sleep(10);
        Assert.fail("failed test 2");
    }
}
