import logging.Log;
import org.testng.Assert;
import org.testng.annotations.Test;
import seleniumCore.CoreGroups;
import seleniumCore.CorePage;
import seleniumCore.DriverFactory;


public class DemoTest1  extends CorePage {

    @Test(groups = {CoreGroups.smokeTests})
    public void DemoTest1_1() {
        DriverFactory.getDriver().get("https://www.youtube.com/");
        Log.info("Just for sake of entering values");
        Assert.assertTrue(true,"failed test 1 apple");
    }

    @Test(groups = {CoreGroups.smokeTests})
    public void DemoTest1_2() {
        DriverFactory.getDriver().get("https://www.facebook.com/");
        Log.info("Just for sake of entering values");
        Assert.assertTrue(true,"failed test 1 apple");
    }
}
