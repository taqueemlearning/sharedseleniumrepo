package reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.*;
import seleniumCore.CoreConstants;
import seleniumCore.CoreProperties;
import seleniumCore.DriverFactory;
import utils.fileReaders.PropertyReader;

import java.io.File;
import java.util.*;


public class ExtentListener implements ITestListener, ISuiteListener {

    private static ExtentReports extentReport;
    public static final ThreadLocal<ExtentTest> extentMethod = new ThreadLocal<>();


    /**
     * This method is invoked before the SuiteRunner starts.
     *
     * @param suite The suite
     */
    @Override
    public void onStart(ISuite suite) {
        HashMap<String, String> propMap = PropertyReader.setProperty(CoreConstants.pathToPropertyFile);
        extentReport = ExtentManager.getInstance(suite.getName());
        extentReport.setSystemInfo(CoreProperties.browserName, System.getProperty(CoreProperties.browserName));
        extentReport.setSystemInfo(CoreProperties.env, System.getProperty(CoreProperties.env));


    }


    /**
     * Invoked before running all the test methods belonging to the classes inside the &lt;test&gt; tag
     * and calling all their Configuration methods.
     *
     * @param context The test context
     */
    @Override
    public synchronized void onStart(ITestContext context) {
    }

    /**
     * Invoked each time before a test will be invoked. The <code>ITestResult</code> is only partially
     * filled with the references to class, method, start millis and status.
     *
     * @param result the partially filled <code>ITestResult</code>
     * @see ITestResult#STARTED
     */
    @Override
    public synchronized void onTestStart(ITestResult result) {
        String className = result.getTestClass().getRealClass().getSimpleName();
        String methodName = result.getMethod().getMethodName();
        extentMethod.set(extentReport.createTest(methodName)
                .assignCategory(className)
                .assignDevice(getDeviceName())
                .assignAuthor(getCategory(result)));
    }

    /**
     * Invoked each time a test succeeds.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#SUCCESS
     */
    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        extentMethod.get().pass(result.getMethod().getMethodName());
    }

    /**
     * Invoked each time a test fails.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#FAILURE
     */
    @Override
    public synchronized void onTestFailure(ITestResult result) {
        String methodName = result.getMethod().getMethodName();
        if (DriverFactory.getDriver() != null) {
            String imagePath = takeScreenShot(DriverFactory.getDriver(), methodName);
            extentMethod.get().fail(result.getThrowable(), MediaEntityBuilder.createScreenCaptureFromPath(imagePath).build());
        } else
            extentMethod.get().fail("Test Case failed");


    }

    /**
     * Invoked each time a test is skipped.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#SKIP
     */
    @Override
    public synchronized void onTestSkipped(ITestResult result) {
        extentMethod.get().skip(result.getMethod().getMethodName());
    }

    /**
     * Invoked each time a method fails but has been annotated with successPercentage and this failure
     * still keeps it within the success percentage requested.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     * @see ITestResult#SUCCESS_PERCENTAGE_FAILURE
     */
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    /**
     * Invoked each time a test fails due to a timeout.
     *
     * @param result <code>ITestResult</code> containing information about the run test
     */
    @Override
    public void onTestFailedWithTimeout(ITestResult result) {

    }


    /**
     * Invoked after all the test methods belonging to the classes inside the &lt;test&gt; tag have run
     * and all their Configuration methods have been called.
     *
     * @param context The test context
     */
    @Override
    public synchronized void onFinish(ITestContext context) {
    }


    /**
     * This method is invoked after the SuiteRunner has run all the tests in the suite.
     *
     * @param suite The suite
     */
    @Override
    public void onFinish(ISuite suite) {
        extentReport.flush();
    }


    public static String takeScreenShot(WebDriver driver, String methodName) {

        String fileLocation = CoreConstants.pathScreenshots + methodName
                + ".png";
        try {
            File scrFile = ((TakesScreenshot) driver)
                    .getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile,
                    new File(fileLocation));
        } catch (Exception e) {
            e.printStackTrace();
        }


        return "../testScreenshots/" + methodName
                + ".png";
    }

    private static String getDeviceName() {
        WebDriver driver = DriverFactory.getDriver();
        String platform;
        String browser;
        String version;
        if (driver != null) {
            Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
//            platform = caps.getPlatformName().toString();
            platform = caps.getPlatform().name();
            browser = caps.getBrowserName();
//            version = caps.getBrowserVersion();
            version = caps.getVersion();
            return platform + "_" + browser + "_" + version;
        } else return System.getProperty(CoreProperties.osName);


    }


    private static String getCategory(ITestResult result) {
        String groupNames;
        List<String> groupName = new ArrayList<>();
        String[] groups = result.getMethod().getGroups();
        if (groups.length > 1) {
            Collections.addAll(groupName, groups);
            groupNames = String.join(" And ", groupName);
        } else if (groups.length == 1)
            groupNames = groups[0];
        else
            groupNames = "No groups";
        return groupNames;
    }

}
