package utils.fileReaders;


import logging.Log;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class PropertyReader {

    public static HashMap<String, String> setProperty(String propertyPath) {
        HashMap<String, String> propertyMap = new HashMap<>();
        Properties properties = new Properties();
        try {
            File propFile = FileUtils.getFile(propertyPath);
            if (propFile.exists()) {
                InputStream inputStream = new FileInputStream(propFile);
                properties.load(inputStream);
                for (String key : properties.stringPropertyNames()) {
                    propertyMap.put(key, properties.getProperty(key));
                    if (System.getProperty(key) == null || System.getProperty(key).isEmpty()) {
                        System.setProperty(key, properties.getProperty(key));
                    }
                }
            } else
                Assert.fail("Please set the path of property File correctly");
        } catch (Exception e) {
            Log.warn("Exception happened during setting system properties");
            e.printStackTrace();
        }
        return propertyMap;
    }

}
