package seleniumCore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.*;
import utils.fileReaders.PropertyReader;

public class CoreTest {

    @BeforeTest
    public void beforeTest() {
        PropertyReader.setProperty(CoreConstants.pathToPropertyFile);
    }

    @BeforeMethod
    @Parameters(value = {"browser"})
    public void beforeMethod(@Optional("optional") String browser) {
        DriverFactory.setDrivers(browser);
        initializePageObjects();
    }

    public WebDriver getDriver() {
        return DriverFactory.getDriver();
    }

    @AfterMethod
    public void afterMethod() {
        DriverFactory.closeBrowser();
    }

    private void initializePageObjects() {
        PageFactory.initElements(getDriver(), this);
    }
}
