package seleniumCore;

import logging.Log;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.InputStream;
import java.time.Duration;
import java.util.Objects;
import java.util.Set;

public class CorePage extends CoreTest {

    public void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void openURL(String url) {
        getDriver().navigate().to(url);
    }
//
//    protected void clickOnElement(WebElement element) {
//        waitTillClickable(element);
//        element.click();
//    }

    protected void sendKeys(WebElement element, String keys) {
        element.clear();
        element.sendKeys(keys);
    }

    public boolean HandleStaleElement(WebElement element) {

        boolean result = false;
        int attempts = 0;
        while (attempts < 2) {
            try {
                element.isDisplayed();
                result = true;
                break;
            } catch (StaleElementReferenceException ignored) {
            }
            attempts++;
        }
        return result;
    }

    public void doubleClickOnElement(WebElement element) {

        Actions action = new Actions(getDriver());
        action.doubleClick(element);
    }

//    public void waitTillClickable(WebElement element) {
//
////        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(30));
//        WebDriverWait wait = new WebDriverWait(getDriver(), 30);
//        wait.until(ExpectedConditions.elementToBeClickable(element));
//
//    }
//
//    public void waitTillVisible(WebElement element) {
//
//        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(30));
//        wait.until(ExpectedConditions.visibilityOf(element));
//    }


    public void handleAlert() {

        Alert alert = getDriver().switchTo().alert();
        alert.accept();
    }

    public void jsClickOnElement(WebElement element) {

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();",
                element);
    }

    public String jsGetInnerText() {

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        return js
                .executeScript("return document.documentElement.innerText;")
                .toString();
    }

    public String jsGetTitle() {

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        return js.executeScript("return document.title;").toString();
    }

    public void jsCreatePopUp() {

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("alert('hello world');");
    }

    public void jsRefresh() {

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("history.go(0)");
    }

    public void jsScrollPage() {

        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        // Vertical scroll - down by 150 pixels
        js.executeScript("window.scrollBy(0,150)");
    }

    public void mouseHoverAndClick(WebElement mainElement,
                                   WebElement subElement) {

        Actions action = new Actions(getDriver());
        action.moveToElement(mainElement).moveToElement(subElement).click()
                .build().perform();
    }

    public void mouseHoverAndClick(WebElement Element) {

        Actions action = new Actions(getDriver());
        action.moveToElement(Element).click().build().perform();
    }

    public void mouseHover(WebElement Element) {

        Actions action = new Actions(getDriver());
        action.moveToElement(Element).build().perform();
    }


    public void rightClickAndSelectOption(WebElement element,
                                          int position) {

        Actions action = new Actions(getDriver());
        action.contextClick(element).build().perform();
        for (int i = 0; i < position; i++)
            action.sendKeys(Keys.ARROW_DOWN).build().perform();
        action.sendKeys(Keys.RETURN).perform();
    }

    public void selectByText(WebElement element, String text) {

        Select dd = new Select(element);
        dd.selectByVisibleText(text);
    }

    public final void dragAndDrop(WebElement source, WebElement target) {

        Actions act = new Actions(getDriver());
        act.dragAndDrop(source, target).perform();
    }

    protected void uploadFileByAutoIT(String fileLocation) {
        if (verifyFileExists(CoreConstants.pathToAutoITChrome + fileLocation)) {
            try {
                Process proc = Runtime.getRuntime().exec(CoreConstants.pathToAutoITChrome + " " + fileLocation);
                InputStream is = proc.getInputStream();
                int retCode = 0;
                while (retCode != -1) {
                    retCode = is.read();
                }
                Log.info("Successfully handled  Upload Window");
            } catch (Exception e) {
                Assert.fail("Exception happened while uploading a file using AUTOIT " + e.getMessage());
            }
        } else
            Assert.fail("The autoit file  was not found at the location : " + fileLocation);


    }


    protected String getDownloadDirectory() {
        String directory = System.getProperty("user.home");
        directory = directory + "\\Downloads\\";
        new File(directory);
        return directory;
    }

    private boolean verifyFileExists(String absFilePath) {
        return !new File(absFilePath).exists();
    }

    protected boolean verifyFileExists(String directory, String fileName) {
        boolean isFileDownloaded = false;
        File filepath = new File(directory);
        File[] contents = filepath.listFiles();
        for (File content : Objects.requireNonNull(contents)) {
            if (content.getName().contains(fileName)) {
                content.delete();
                isFileDownloaded = true;
            }
        }
        return isFileDownloaded;
    }

    protected boolean verifyElementIsPresent(WebElement element) {
        return element.isDisplayed();
    }

    protected boolean verifyElementHasText(WebElement element, String expectedText) {
        return element.getText().contains(expectedText);
    }

//    protected void waitTillElementNotDisplayed(WebElement element, int seconds) {
//        WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofSeconds(seconds));
//        wait.until(ExpectedConditions.invisibilityOf(element));
//
//    }

    protected void closeChildWindows() {
        String parent = getDriver().getWindowHandle();
        Set<String> pops = getDriver().getWindowHandles();
        for (String pop : pops) {
            if (!pop.contains(parent)) {
                getDriver().switchTo().window(pop);
                getDriver().close();
            }
        }
    }
}
