package seleniumCore;

public interface CoreConstants {

    String pathToPropertyFile = "../CoreProject/src/test/resources/properties/coreConfig.properties";
    String pathToDrivers = "../CoreProject/src/test/resources/drivers";
    String pathToExtentReports = "../CoreProject/src/test/resources/extentReports/testReports/";
    String pathScreenshots = "../CoreProject/src/test/resources/extentReports/testScreenshots/";
    String firefox = "firefox";
    String chrome = "chrome";
    String appium = "appium";
    String ie = "ie";
    String edge = "edge";
    String safari = "safari";
    String os_mac = "Mac";
    String os_windows = "Windows";
    String os_linux = "Linux";
    String pathToAutoITChrome = "../CoreProject/src/main/resources/autoITFiles/UploadInChrome.exe";
}
