package seleniumCore;

import enums.OSType;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AndroidBy;
import logging.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    private static final Map<Integer, WebDriver> driverMap = new HashMap<>();

    private static OptionsManager optionsManager = new OptionsManager();

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private static ThreadLocal<String> selectedBrowser = new ThreadLocal<String>();

    private DriverFactory() {

    }


    public static synchronized void setDrivers(String driver) {
        String driverInPropertyFile = System.getProperty(CoreProperties.browserName);
        if (driver.contentEquals("optional")) {
            if (driverInPropertyFile.isEmpty())
                driver = "chrome";
            else
                driver = driverInPropertyFile;
        }
        if (System.getProperty(CoreProperties.hubAddress) != null) {
            if (!System.getProperty(CoreProperties.hubAddress).isEmpty()) {
                setRemoteDriver(driver);
            }
        } else {
            setLocalDriver(driver);
        }
        setTimeOuts();


    }

    private static synchronized void setRemoteDriver(String browser) {
        String hubAddress = System.getProperty(CoreProperties.hubAddress);
        try {
            if (System.getProperty(CoreProperties.osType).equalsIgnoreCase(OSType.MOBILE.toString())) {
                driver.set(new AppiumDriver(new URL(hubAddress), optionsManager.getAppiumOptions()));
            } else if (System.getProperty(CoreProperties.osType).equalsIgnoreCase(OSType.DESKTOP.toString())) {
                if (browser.equalsIgnoreCase(CoreConstants.firefox)) {
                    driver.set(new RemoteWebDriver(new URL(hubAddress), optionsManager.getFirefoxOptions()));
                } else if (browser.equalsIgnoreCase(CoreConstants.chrome)) {
                    driver.set(new RemoteWebDriver(new URL(hubAddress), optionsManager.getChromeOptions()));
                } else if (browser.equalsIgnoreCase(CoreConstants.ie)) {
                    driver.set(new RemoteWebDriver(new URL(hubAddress), optionsManager.getIEOptions()));
                } else if (browser.equalsIgnoreCase(CoreConstants.edge)) {
                    driver.set(new RemoteWebDriver(new URL(hubAddress), optionsManager.getEdgeOptions()));
                } else
                    Assert.fail("Please choose a desktop browser to run a test");
            } else {
                Assert.fail("Please choose a valid OSTYPE , either desktop or mobile");
            }
        } catch (Exception e) {
            Log.warn("Exception Happened in Hub node configuration");
            e.printStackTrace();
            Assert.fail("Check the hub node configuration");
        }
    }

    private static synchronized void setLocalDriver(String browser) {

        String execPath = getDriverExecutables(browser);

        if (browser.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", execPath);
            driver.set(new FirefoxDriver(optionsManager.getFirefoxOptions()));
            selectedBrowser.set(browser);

        } else if (browser.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", execPath);
            driver.set(new ChromeDriver(optionsManager.getChromeOptions()));
            selectedBrowser.set(browser);

        } else if (browser.equals("ie")) {
            System.setProperty("webdriver.ie.driver", execPath);
            driver.set(new InternetExplorerDriver(optionsManager.getIEOptions()));
            selectedBrowser.set(browser);

        } else if (browser.equals("edge")) {
            System.setProperty("webdriver.edge.driver", execPath);
            driver.set(new EdgeDriver(optionsManager.getEdgeOptions()));
            selectedBrowser.set(browser);

        } else {
            Log.warn("No browser is mentioned ");
            Assert.fail("Unable to create a driver");
        }
        driverMap.put((int) Thread.currentThread().getId(), driver.get());
    }

    public static synchronized WebDriver getDriver() {
        return driver.get();
    }

    public static synchronized String getBrowserName() {
        return selectedBrowser.get();
    }

    public static synchronized void closeBrowser() {

        getDriver().quit();
        selectedBrowser.set(null);
    }

    private static synchronized void setTimeOuts() {

//        getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        getDriver().manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
//        getDriver().manage().timeouts().setScriptTimeout(Duration.ofSeconds(30));
    }


    private static String getDriverExecutables(String driver) {
        String execPath = null;

        String os = System.getProperty(CoreProperties.osName);
        if (os.contains(CoreConstants.os_mac))
            execPath = CoreConstants.pathToDrivers + "/" + CoreConstants.os_mac;
        else if (os.contains(CoreConstants.os_windows))
            execPath = CoreConstants.pathToDrivers + "/" + CoreConstants.os_windows;
        else if (os.contains(CoreConstants.os_linux))
            execPath = CoreConstants.pathToDrivers + "/" + CoreConstants.os_linux;
        else
            Assert.fail("Supporting MAC, Windows and Linux as of now., Please contact Taqueem");

        if (driver.equalsIgnoreCase(CoreConstants.chrome))
            if (os.contains(CoreConstants.os_windows))
                execPath = execPath + "/chromedriver.exe";
            else
                execPath = execPath + "/chromedriver";
        if (driver.equalsIgnoreCase(CoreConstants.firefox))
            if (os.contains(CoreConstants.os_windows))
                execPath = execPath + "/geckodriver.exe";
            else
                execPath = execPath + "/geckodriver";
        if (driver.equalsIgnoreCase(CoreConstants.edge))
            if (os.contains(CoreConstants.os_windows))
                execPath = execPath + "/msedgedriver.exe";
            else
                execPath = execPath + "/msedgedriver";
        return execPath;

    }
}
